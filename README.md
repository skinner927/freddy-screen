# Freddy's Screen

This project targets Python 3.7.3

This was built with a Raspberry Pi 3 A+ 512MB ram and a
[Adafruit RGB Bonnet](https://www.adafruit.com/product/3211).

Adafruit has a
[setup guide](https://learn.adafruit.com/adafruit-rgb-matrix-bonnet-for-raspberry-pi/).
The most important part is step #6 on the
[Driving Matrices](https://learn.adafruit.com/adafruit-rgb-matrix-bonnet-for-raspberry-pi/driving-matrices)
page for setting up the Python library with defaults for the bonnet.
(Choose "Quality (disables sound, requires soldering)" when asked)

The library that runs the whole thing is
[hzeller/rpi-rgb-led-matrix](https://github.com/hzeller/rpi-rgb-led-matrix).
There are C, C++, and Python bindings. We're only using Python3 ATM.

I used two 
[64x64 pixel 160x160mm 1/32 scan P2.5 (2.5mm pitch) indoor full color LED displays](https://www.aliexpress.com/item/32830509434.html?spm=a2g0s.9042311.0.0.16f04c4dt5LJ3i)
and a generic 
[5v 20A power supply](https://www.aliexpress.com/item/32887234626.html?spm=a2g0s.9042311.0.0.16f04c4dt5LJ3i).
20A may be a bit overkill, but Adafruit implied that one panel uses 7A so I'm not sure.

I powered the Pi directly from the power supply, but had to tune the power
supply to output a bit more voltage so the Pi didn't complain about being under
powered (lightning bolt in the corner of the screen). Currently the power supply
is outputting a constant 5.25v without load and this seemed to make everyone
happy.

Use `vcgencmd measure_volts core` to get the core voltage which should be around
~1.2 - 1.4v. `volt=1.3625V` seems to be nice.
And finally to see if you're being throttled: `vcgencmd get_throttled`.
`0x0` means you're in the clear.

## Bonnet modifications

I soldered the `(E)` jumper (with `8` and `16` options) to `8`.

I also soldered a jumper between `GPIO4` and `GPIO18` which helps the screen
flicker less at the sacrifice of no on-board audio (we didn't use it anyways).

## Switching to DietPi

- Copy `dietpi.txt` and `dietpi_config.txt` to the flash drive's `dietpi.txt`
  and `config.txt` respectively. Basically turn everything off.

- Configure WiFi with `dietpi-wifi.txt`

- Reserve a CPU core: https://github.com/hzeller/rpi-rgb-led-matrix#cpu-use

  add `isolcpus=3` to the first line in `/boot/cmdline.txt`

- Install base packages:

  ```
  sudo apt-get install -y build-essential python3-pip
  ```

## Preparing the Pi

I used Raspbian Buster Lite (version 10; 2019-07-10; Kernel 4.19). 
This is a headless install.

I also uninstalled Bluetooth packages to help speed things up.
```
sudo apt-get remove bluez bluez-firmware pi-bluetooth
```

The following lines were added/modified to `/boot/config.txt`. This can be
done directly from the SD card before booting. Some of these values may already
exist so ensure you search the file for every setting before specifying a new
value.

- Ensure these lines are commented out if they exist 
```
#dtoverlay=vc4-fkms-v3d
#dtoverlay=w1-gpio
```
- Set: 
```
gpiopin=19
start_x=0
enable_uart=0

# Disable Audio as it causes flickers
dtparam=audio=off
```
- Limit display resolution and GPU ram since we're headless
```
# No HDMI audio
hdmi_drive=1
# DMT mode (monitors not TVs)
hdmi_group=2
# 1024x768 @ 60 Hz
hdmi_mode=16
# Minimum memory for the gpu
gpu_mem=64
```

- Disable services
```
sudo update-rc.d bluetooth disable
```
 
- Install Python deps
```
# You seemingly have to do it this way or it doesn't work
sudo python3 -m pip install -r requirements.txt
``` 

# WiFi

Either run `sudo raspi-config` to change the WiFi and reboot or manually edit
`/etc/wpa_supplicant/wpa_supplicant.conf`.


## Fonts
Fonts are copied directly from the rpi-rgb-led-matrix repo. 


## IG Logo
Comes from: http://pixelartmaker.com/art/31c29e1581fabdf
but I converted it to actual pixel.
