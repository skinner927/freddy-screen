#!/usr/bin/env python3
import argparse
import datetime
import functools
import logging
import os
import queue
import re
import socket
import threading
import time

import pytz
import requests
from bs4 import BeautifulSoup
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from rgbmatrix import graphics
from PIL import Image

# TODO: Load this from a config file
local_tz = pytz.timezone("America/New_York")
loop_sleep = 1
check_ig = 1800  # 1800s = 30min

follower_re = re.compile(r"([0-9,]+) Followers")


def get_follower_count(max_retries=2, _retry_count=0):
    try:
        r = requests.get("https://www.instagram.com/south_tampa_woodworks/")
    except Exception:
        if max_retries >= _retry_count:
            logging.exception("Could not get IG URL")
            return None
        logging.warning("Could not get IG URL but trying again")
        return get_follower_count(
            max_retries=max_retries, _retry_count=_retry_count + 1
        )

    if r.status_code != 200:
        return None
    soup = BeautifulSoup(r.content, features="html.parser")
    meta = list(soup.find_all("meta", property="og:description"))
    if len(meta) < 1:
        meta = filter(lambda x: x.get("description", None), soup.find_all("meta"))
    if len(meta) < 1:
        return None
    content = meta[0].get("content", "")
    match = follower_re.match(content)
    if not match:
        return None
    followers = match.group(0) or None
    # Strip the "Followers" part off
    if followers:
        # len(" followers") == 10
        followers = followers[:-10]
    return followers


def start_update_thread(halt, wait, update_q):
    while True:
        wait.wait()
        if halt.is_set():
            return
        ig_followers = get_follower_count()
        if ig_followers:
            update_q.put(ig_followers)
        wait.clear()


def text_width(font, text):
    return sum([font.CharacterWidth(ord(c)) for c in text])


class Location(object):
    __slots__ = ("city", "tz", "font", "color", "city_width", "time_format")

    def __init__(self, city, tz, display_24, font, color):
        self.city = city
        self.tz = tz
        self.font = font
        self.color = color
        # Precalc
        self.city_width = self.text_width(city)
        if display_24:
            self.time_format = "%-H:%M"
        else:
            self.time_format = "%-I:%M %p"

    def text_width(self, text):
        return text_width(self.font, text)

    def write(self, canvas, y):
        # Write the city
        graphics.DrawText(canvas, self.font, 0, y, self.color, self.city)

        # Build the clock
        current = datetime.datetime.now().astimezone(self.tz)
        clock = current.strftime(self.time_format)

        # Figure out how much extra space we have
        clock_width = self.text_width(clock)
        fill = canvas.width - (self.city_width + clock_width)
        # We'll fill with literal pixel gaps instead of space characters
        clock_start = self.city_width + max(0, fill)

        # Write the clock
        graphics.DrawText(canvas, self.font, clock_start, y, self.color, clock)


class RunText(object):
    def __init__(self):
        options, args = self.get_options()

        self.skip_network = args.skip_network

        self.matrix = RGBMatrix(options=options)

        self.update_q = queue.Queue()
        self.trigger_update = threading.Event()
        self.update_thread_halt = threading.Event()
        self.update_thread = threading.Thread(
            target=start_update_thread,
            kwargs=dict(
                halt=self.update_thread_halt,
                wait=self.trigger_update,
                update_q=self.update_q,
            ),
            daemon=True,
        )
        self.update_thread.start()

        pwd = os.path.dirname(os.path.abspath(__file__))
        self.diag_font = graphics.Font()
        self.diag_font.LoadFont(os.path.join(pwd, "fonts", "5x8.bdf"))

        self.ig_font = graphics.Font()
        self.ig_font.LoadFont(os.path.join(pwd, "fonts", "9x18B.bdf"))
        self.ig_color = graphics.Color(102, 51, 0)

        self.ig_logo = Image.open(os.path.join(pwd, "img", "ig.gif"))
        self.ig_logo = self.ig_logo.convert("RGB")

        common_font = graphics.Font()
        common_font.LoadFont(os.path.join(pwd, "fonts", "7x13B.bdf"))
        self.common_font = common_font

        # TODO: Load locations from a config file
        display_24 = False  # This should be global
        self.locations = [
            Location(
                "Local",
                pytz.timezone("America/New_York"),
                display_24,
                common_font,
                graphics.Color(255, 153, 51),
            ),
            Location(
                "Melbourne",
                pytz.timezone("Australia/Melbourne"),
                display_24,
                common_font,
                graphics.Color(255, 153, 204),
            ),
            Location(
                "Manila",
                pytz.timezone("Asia/Manila"),
                display_24,
                common_font,
                graphics.Color(102, 255, 102),
            ),
        ]

    @staticmethod
    def get_options():
        # Pulled from:
        # https://github.com/hzeller/rpi-rgb-led-matrix/blob/master/bindings/python/samples/samplebase.py
        parser = argparse.ArgumentParser()

        parser.add_argument(
            "-r",
            "--led-rows",
            action="store",
            help="Display rows. 16 for 16x32, 32 for 32x32. Default: 64",
            default=64,
            type=int,
        )
        parser.add_argument(
            "--led-cols",
            action="store",
            help="Panel columns. Typically 32 or 64. Default: 64",
            default=64,
            type=int,
        )
        parser.add_argument(
            "-c",
            "--led-chain",
            action="store",
            help="Daisy-chained boards. Default: 2.",
            default=2,
            type=int,
        )
        parser.add_argument(
            "-P",
            "--led-parallel",
            action="store",
            help="For Plus-models or RPi2: parallel chains. 1..3. Default: 1",
            default=1,
            type=int,
        )
        parser.add_argument(
            "-p",
            "--led-pwm-bits",
            action="store",
            help="Bits used for PWM. Something between 1..11. Default: 11",
            default=11,
            type=int,
        )
        parser.add_argument(
            "-b",
            "--led-brightness",
            action="store",
            help="Sets brightness level. Default: 40. Range: 1..100",
            default=40,
            type=int,
        )
        parser.add_argument(
            "-m",
            "--led-gpio-mapping",
            help="Hardware Mapping: regular, adafruit-hat, adafruit-hat-pwm",
            choices=["regular", "adafruit-hat", "adafruit-hat-pwm"],
            type=str,
        )
        parser.add_argument(
            "--led-scan-mode",
            action="store",
            help="Progressive or interlaced scan. 0 Progressive, 1 Interlaced "
            "Default: 1",
            default=1,
            choices=range(2),
            type=int,
        )
        parser.add_argument(
            "--led-pwm-lsb-nanoseconds",
            action="store",
            help="Base time-unit for the on-time in the lowest significant bit in "
            "nanoseconds. Default: 130",
            default=130,
            type=int,
        )
        parser.add_argument(
            "--led-show-refresh",
            action="store_true",
            help="Shows the current refresh rate of the LED panel",
        )
        parser.add_argument(
            "--led-slowdown-gpio",
            action="store",
            help="Slow down writing to GPIO. Range: 0..4. Default: 2",
            default=2,
            type=int,
        )
        parser.add_argument(
            "--led-no-hardware-pulse",
            action="store",
            help="Don't use hardware pin-pulse generation",
        )
        parser.add_argument(
            "--led-rgb-sequence",
            action="store",
            help="Switch if your matrix has led colors swapped. Default: RGB",
            default="RGB",
            type=str,
        )
        parser.add_argument(
            "--led-pixel-mapper",
            action="store",
            help='Apply pixel mappers. e.g "Rotate:90"',
            default="",
            type=str,
        )
        parser.add_argument(
            "--led-row-addr-type",
            action="store",
            help="0 = default; 1=AB-addressed panels;2=row direct",
            default=0,
            type=int,
            choices=[0, 1, 2],
        )
        parser.add_argument(
            "--led-multiplexing",
            action="store",
            help="Multiplexing type: 0=direct; 1=strip; 2=checker; 3=spiral; "
            "4=ZStripe; 5=ZnMirrorZStripe; 6=coreman; 7=Kaler2Scan; "
            "8=ZStripeUneven (Default: 0)",
            default=0,
            type=int,
        )
        parser.add_argument(
            "--drop-privileges",
            action="store_true",
            help="Include to drop root privileges after startup",
        )
        parser.add_argument(
            "--skip-network", action="store_true", help="Skip network checking"
        )

        args = parser.parse_args()
        options = RGBMatrixOptions()

        if args.led_gpio_mapping is not None:
            options.hardware_mapping = args.led_gpio_mapping
        options.rows = args.led_rows
        options.cols = args.led_cols
        options.chain_length = args.led_chain
        options.parallel = args.led_parallel
        options.row_address_type = args.led_row_addr_type
        options.multiplexing = args.led_multiplexing
        options.pwm_bits = args.led_pwm_bits
        options.brightness = args.led_brightness
        options.pwm_lsb_nanoseconds = args.led_pwm_lsb_nanoseconds
        options.led_rgb_sequence = args.led_rgb_sequence
        options.pixel_mapper_config = args.led_pixel_mapper
        if args.led_show_refresh:
            options.show_refresh_rate = 1

        if args.led_slowdown_gpio is not None:
            options.gpio_slowdown = args.led_slowdown_gpio
        if args.led_no_hardware_pulse:
            options.disable_hardware_pulsing = True

        options.drop_privileges = args.drop_privileges

        return options, args

    def cleanup(self):
        self.update_thread_halt.set()
        self.trigger_update.set()
        self.update_thread.join(timeout=2.0)

    @staticmethod
    def get_ip():
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                # Google's DNS
                sock.connect(("8.8.8.8", 53))
                ip = sock.getsockname()[0]
                if not ip.startswith("127."):
                    return ip
        except Exception:
            logging.exception("Could not get IP from OS")

        return None

    def wait_for_network(self, timeout=30):
        write = functools.partial(
            graphics.DrawText,
            self.matrix,
            self.diag_font,
            0,
            self.diag_font.height,
            graphics.Color(255, 51, 51),
        )

        loop = 1
        while True:
            ip = self.get_ip()
            self.matrix.Clear()

            if ip:
                write("IP: {}".format(ip))
                time.sleep(2)
                return

            write("No IP Address {}".format(loop))
            loop += 1
            if loop >= timeout:
                return
            time.sleep(1)

    def draw_ig_logo(self, matrix, x=0, y=0):
        # By default it lands in the bottom left corner
        logo_width, logo_height = self.ig_logo.size
        y_offset = self.matrix.height - logo_height
        matrix.SetPixelsPillow(x, y_offset + y, logo_width, logo_height, self.ig_logo)

    def run(self):
        if not self.skip_network:
            self.wait_for_network()

        do_check_ig = int(check_ig / loop_sleep)
        loop_count = 0

        ig_pad = self.ig_logo.size[1] + 1
        ig_followers = None
        while True:
            buffer = self.matrix.CreateFrameCanvas()

            # Follower count update
            try:
                ig_followers = self.update_q.get(block=False)
                loop_count = 0
            except queue.Empty:
                # Do we trigger periodic updates?
                if not ig_followers or loop_count > do_check_ig:
                    loop_count = 0
                    self.trigger_update.set()

            # Print times
            for i, loc in enumerate(self.locations):
                y = (i + 1) * self.common_font.height
                y -= 2  # Fudge
                loc.write(buffer, y)

            # Print IG
            y_start = (
                len(self.locations) * self.common_font.height
            ) + self.ig_font.height
            self.draw_ig_logo(buffer)
            graphics.DrawText(
                buffer, self.ig_font, ig_pad, y_start, self.ig_color, "STW"
            )
            followers = str(ig_followers) if ig_followers else "??"
            graphics.DrawText(
                buffer,
                self.ig_font,
                self.matrix.width - text_width(self.ig_font, followers),
                y_start,
                self.ig_color,
                followers,
            )

            self.matrix.SwapOnVSync(buffer)
            loop_count += 1
            time.sleep(loop_sleep)


# Main function
if __name__ == "__main__":
    logging.basicConfig()
    run_text = RunText()
    try:
        run_text.run()
    except KeyboardInterrupt:
        pass
    finally:
        run_text.cleanup()
